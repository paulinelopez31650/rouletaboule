## Gitlab

Cloner le projet:
git clone git clone git@gitlab.com:paulinelopez31650/rouletaboule.git
cd rouletaboule,

## Les maquettes

Elles se trouvent dans le dossier maquettes.

## Installation
- Bootstrap via CDN,
- npm install parcel-bundler,
- npm init,

##  Version en ligne :
avec Surge
lien: http://rouletaboule.surge.sh/

## La performance Score:
Le diagnostic performance est de 94%
https://developers.google.com/speed/pagespeed/insights/?hl=fr&url=http%3A%2F%2Fportfoliodepaulinelopez.surge.sh%2F

## Eco-Index:
L'ecoindex est A
http://www.ecoindex.fr/resultats/?id=115234
http://www.ecoindex.fr/